# cloud.google.com/go v0.34.0
## explicit
# github.com/alcortesm/tgz v0.0.0-20161220082320-9c5fe88206d7
## explicit
# github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239
## explicit
# github.com/armon/go-socks5 v0.0.0-20160902184237-e75332964ef5
## explicit
# github.com/blang/semver v3.5.1+incompatible
## explicit
github.com/blang/semver
# github.com/creack/pty v1.1.7
## explicit
# github.com/davecgh/go-spew v1.1.1
## explicit
# github.com/emirpasic/gods v1.12.0
## explicit
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/lists
github.com/emirpasic/gods/lists/arraylist
github.com/emirpasic/gods/trees
github.com/emirpasic/gods/trees/binaryheap
github.com/emirpasic/gods/utils
# github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568
## explicit
# github.com/gliderlabs/ssh v0.2.2
## explicit
# github.com/golang/protobuf v1.3.0
## explicit
github.com/golang/protobuf/proto
# github.com/google/go-cmp v0.3.1
## explicit; go 1.8
# github.com/google/go-querystring v1.0.0
## explicit
github.com/google/go-querystring/query
# github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
## explicit
github.com/jbenet/go-context/io
# github.com/jessevdk/go-flags v1.4.0
## explicit
# github.com/juranki/go-semrel v0.0.0-20190901152454-aa753593507a
## explicit
github.com/juranki/go-semrel/angularcommit
github.com/juranki/go-semrel/inspectgit
github.com/juranki/go-semrel/semrel
# github.com/kevinburke/ssh_config v0.0.0-20190725054713-01f96b0aa0cd
## explicit
github.com/kevinburke/ssh_config
# github.com/kr/pretty v0.1.0
## explicit
# github.com/kr/pty v1.1.8
## explicit; go 1.12
# github.com/kr/text v0.1.0
## explicit
# github.com/mitchellh/go-homedir v1.1.0
## explicit
github.com/mitchellh/go-homedir
# github.com/pelletier/go-buffruneio v0.2.0
## explicit
# github.com/pkg/errors v0.8.1
## explicit
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
## explicit
# github.com/sergi/go-diff v1.0.0
## explicit
github.com/sergi/go-diff/diffmatchpatch
# github.com/src-d/gcfg v1.4.0
## explicit
github.com/src-d/gcfg
github.com/src-d/gcfg/scanner
github.com/src-d/gcfg/token
github.com/src-d/gcfg/types
# github.com/stretchr/objx v0.2.0
## explicit; go 1.12
# github.com/stretchr/testify v1.3.0
## explicit
# github.com/urfave/cli v1.20.0
## explicit
github.com/urfave/cli
# github.com/xanzy/go-gitlab v0.16.0
## explicit
github.com/xanzy/go-gitlab
# github.com/xanzy/ssh-agent v0.2.1
## explicit
github.com/xanzy/ssh-agent
# golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
## explicit
golang.org/x/crypto/cast5
golang.org/x/crypto/curve25519
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
golang.org/x/crypto/internal/chacha20
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/openpgp
golang.org/x/crypto/openpgp/armor
golang.org/x/crypto/openpgp/elgamal
golang.org/x/crypto/openpgp/errors
golang.org/x/crypto/openpgp/packet
golang.org/x/crypto/openpgp/s2k
golang.org/x/crypto/poly1305
golang.org/x/crypto/ssh
golang.org/x/crypto/ssh/agent
golang.org/x/crypto/ssh/knownhosts
# golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
## explicit; go 1.11
golang.org/x/net/context
golang.org/x/net/context/ctxhttp
golang.org/x/net/internal/socks
golang.org/x/net/proxy
# golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
## explicit; go 1.11
golang.org/x/oauth2
golang.org/x/oauth2/internal
# golang.org/x/sync v0.0.0-20190423024810-112230192c58
## explicit
# golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a
## explicit; go 1.12
golang.org/x/sys/cpu
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.2
## explicit
# golang.org/x/tools v0.0.0-20190813142322-97f12d73768f
## explicit; go 1.11
# golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
## explicit; go 1.11
# google.golang.org/appengine v1.4.0
## explicit
google.golang.org/appengine/internal
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
# google.golang.org/genproto v0.0.0-20180831171423-11092d34479b
## explicit
# gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
## explicit
# gopkg.in/src-d/go-billy.v4 v4.3.2
## explicit
gopkg.in/src-d/go-billy.v4
gopkg.in/src-d/go-billy.v4/helper/chroot
gopkg.in/src-d/go-billy.v4/helper/polyfill
gopkg.in/src-d/go-billy.v4/osfs
gopkg.in/src-d/go-billy.v4/util
# gopkg.in/src-d/go-git-fixtures.v3 v3.5.0
## explicit
# gopkg.in/src-d/go-git.v4 v4.13.1
## explicit
gopkg.in/src-d/go-git.v4
gopkg.in/src-d/go-git.v4/config
gopkg.in/src-d/go-git.v4/internal/revision
gopkg.in/src-d/go-git.v4/internal/url
gopkg.in/src-d/go-git.v4/plumbing
gopkg.in/src-d/go-git.v4/plumbing/cache
gopkg.in/src-d/go-git.v4/plumbing/filemode
gopkg.in/src-d/go-git.v4/plumbing/format/config
gopkg.in/src-d/go-git.v4/plumbing/format/diff
gopkg.in/src-d/go-git.v4/plumbing/format/gitignore
gopkg.in/src-d/go-git.v4/plumbing/format/idxfile
gopkg.in/src-d/go-git.v4/plumbing/format/index
gopkg.in/src-d/go-git.v4/plumbing/format/objfile
gopkg.in/src-d/go-git.v4/plumbing/format/packfile
gopkg.in/src-d/go-git.v4/plumbing/format/pktline
gopkg.in/src-d/go-git.v4/plumbing/object
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp/capability
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp/sideband
gopkg.in/src-d/go-git.v4/plumbing/revlist
gopkg.in/src-d/go-git.v4/plumbing/storer
gopkg.in/src-d/go-git.v4/plumbing/transport
gopkg.in/src-d/go-git.v4/plumbing/transport/client
gopkg.in/src-d/go-git.v4/plumbing/transport/file
gopkg.in/src-d/go-git.v4/plumbing/transport/git
gopkg.in/src-d/go-git.v4/plumbing/transport/http
gopkg.in/src-d/go-git.v4/plumbing/transport/internal/common
gopkg.in/src-d/go-git.v4/plumbing/transport/server
gopkg.in/src-d/go-git.v4/plumbing/transport/ssh
gopkg.in/src-d/go-git.v4/storage
gopkg.in/src-d/go-git.v4/storage/filesystem
gopkg.in/src-d/go-git.v4/storage/filesystem/dotgit
gopkg.in/src-d/go-git.v4/storage/memory
gopkg.in/src-d/go-git.v4/utils/binary
gopkg.in/src-d/go-git.v4/utils/diff
gopkg.in/src-d/go-git.v4/utils/ioutil
gopkg.in/src-d/go-git.v4/utils/merkletrie
gopkg.in/src-d/go-git.v4/utils/merkletrie/filesystem
gopkg.in/src-d/go-git.v4/utils/merkletrie/index
gopkg.in/src-d/go-git.v4/utils/merkletrie/internal/frame
gopkg.in/src-d/go-git.v4/utils/merkletrie/noder
# gopkg.in/warnings.v0 v0.1.2
## explicit
gopkg.in/warnings.v0
